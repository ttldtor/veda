import std.stdio;
import derelict.sdl2.sdl;
import core.thread;

void main() {
	DerelictSDL2.load(SharedLibVersion(2, 0, 2));

	SDL_Window* window;

	SDL_Init(SDL_INIT_VIDEO);

    window = SDL_CreateWindow("Veda", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480,
        SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

    if (!window) {
        printf("Could not create window: %s\n", SDL_GetError());
        return;
    }

    while (1) {
        SDL_Event e;
        if (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT) {
                break;
            }
        }

        Thread.sleep(dur!("msecs")(13));
    }

    SDL_DestroyWindow(window);
    SDL_Quit();
}
